import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import reportWebVitals from "./reportWebVitals";
import { createStore, applyMiddleware, compose } from "redux";
import { Provider } from "react-redux";
import thunk from 'redux-thunk'
import reducers from "./reducers";


import {
  BrowserRouter,
  Routes,
  Route
} from 'react-router-dom';
import { LandingPage, Login, Protected, RecipeReviewCard, ListCars, About } from "./components";

const store = createStore(reducers, compose(applyMiddleware(thunk)))

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <BrowserRouter>
    <Routes>
      <Route path='/web' element={
            <Protected >
              <Provider store={store}>
                <LandingPage/>
                <ListCars/>
              </Provider>
            </Protected>
          }></Route>
      <Route path='/about' element={
            <Protected >
              <About/>
            </Protected>
          }></Route>
      <Route path='/blog' element={
            <Protected>
                <RecipeReviewCard/>
            </Protected>
          }></Route>
      <Route path="/" element={
          <Login/>
      }/>
    </Routes>
  </BrowserRouter>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();