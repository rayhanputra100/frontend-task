import { useState, useEffect } from "react";
import logo from "./logo.svg";
import "./App.css";
import {GoogleLogin} from 'react-google-login'
import { Navigate } from "react-router-dom";

async function doLogin({ email, password }) {
  // Gunakan endpoint-mu sendiri
  const response = await fetch("http://localhost:8000/api/v1/login/", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      email,
      password,
    }),
  });
  const data = await response.json();
  return data.token;
}

async function doLoginGoogle({ email, password, token }) {
  // Gunakan endpoint-mu sendiri
  const response = await fetch("http://localhost:8000/api/v1/google", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      email,
      password,
      token
    }),
  });
  const data = await response.json();
  return data.token;
}


function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const token = localStorage.getItem("token");
  const GOOGLECLIENTID = "979909028362-m77kbte3cel9jompo35lvn4fsg3vv882.apps.googleusercontent.com"

  console.log(email, password)

  useEffect(() => {
    setIsLoggedIn(!!token);
  }, [token]);

  function handleSubmit(e) {
    setIsLoading(true);
    e.preventDefault();
    doLogin({ email, password })
      .then((token) => localStorage.setItem("token", token))
      .catch((err) => console.log(err.message))
      .finally(() => setIsLoading(false));
  }

  const haldleSuccessGoogle = (response) => {
    console.log(response);
    console.log(response.tokenId);
    if(response.tokenId) {
      doLoginGoogle(response.tokenId)
        .then((token) => {
            localStorage.setItem("token", token);
            setIsLoggedIn(token);
          })
        .catch((err) => console.log(err.message))
        .finally(() => setIsLoading(false));      
    }
  }

  const haldleFailureGoogle = (response) => {
    console.log(response);
    alert(response);
  }

  // function handleLogout(e) {
  //   setIsLoading(true);
  //   e.preventDefault();
  //   localStorage.removeItem("token");
  //   setIsLoggedIn(false);
  //   setIsLoading(false);
  // }

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />

        {!isLoggedIn ? (
          <form onSubmit={handleSubmit}>
            <input
              type="email"
              placeholder="Email"
              onChange={(e) => setEmail(e.target.value)}
              value={email}
            />
            <input
              type="password"
              placeholder="Password"
              onChange={(e) => setPassword(e.target.value)}
              value={password}
            />
            <input type="submit" value={isLoading ? "Loading" : "Login"} />
            
          </form>
        ) : (
          <Navigate to="/web"></Navigate>
        )}
        <GoogleLogin
              clientId={GOOGLECLIENTID}
              buttonText="Login with Google"
              onSuccess={haldleSuccessGoogle}
              onFailure={haldleFailureGoogle}
              cookiePolicy={'single_host_origin'}
            />
      </header>
    </div>
  );
}

export default Login;
