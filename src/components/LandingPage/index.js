import Header from "../Header";
import Cars from "../cardCars";

function LandingPage(){
	return (
		<>
			<Header/>
			<Cars/>
		</>
	)
}

export default LandingPage