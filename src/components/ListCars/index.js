import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getListCars } from "../../actions/listCarAction";

function ListCars() {
    
  const { getListCarsResult, getListCarsLoading, getListCarsError } =
    useSelector((state) => state.car);

  const dispatch = useDispatch();

  useEffect(() => {
    //panggil get List Cars
    dispatch(getListCars());
  }, [dispatch]);


  return (
    <div className="row d-flex justify-content-center" id="cars-container">
      {getListCarsResult ? (
        getListCarsResult.map((car) => {
            return (
                    <div className="col-md-3" key={car.id}>
                        <div className="card mb-3">
                            <img src={car.image} className="card-img-top" height="350" alt={car.manufacture}/>
                            <div className="card-body">
                                <p className="card-text">{car.model}</p>
                                <h5 className="card-title">Rp.{car.rentPerDay} / Hari</h5>
                                <p className="card-text">{car.description}</p>
                                <p className="card-text"><img src="/assets/images/fi_users.png" className="me-2" alt="Users"/>{car.capacity}</p>
                                <p className="card-text"><img src="assets/images/fi_settings.png" className="me-2" alt="Transmission"/>{car.transmission}</p>
                                <p className="card-text"><img src="assets/images/fi_calendar.png" className="me-2" alt="Calender"/>{car.year}</p>
                                <a href="/" className="btn btn-primary d-flex justify-content-center">Navigate Pilih Mobil</a>
                            </div>
                        </div>
                    </div>
            )
        })
      ) : getListCarsLoading ? (
        <p>Loading . . . </p>
      ) : (
        <p>{getListCarsError ? getListCarsError : "Data Kosong"}</p>
      )}
    </div>
  );
}

export default ListCars;
