import React from 'react';

const About = () => {
    return (
        <div>
            <header class="bgHeader">
                <div class="container-fluid mb-5">
                    <nav class="navbar navbar-expand-lg bg-putih pt-3">
                        <div class="container rNav">
                            <a class="navbar-brand text-light px-4" style={{backgroundColor: `#0d28a6`}} href="/">Hanz</a>
                            <button class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasWithBothOptions" aria-controls="offcanvasWithBothOptions" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                            <div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvasWithBothOptions" aria-labelledby="offcanvasWithBothOptionsLabel">
                                <div class="offcanvas-header">
                                    <h5 class="offcanvas-title" id="offcanvasWithBothOptionsLabel"><a class="navbar-brand text-light px-4" style={{backgroundColor: `#0d28a6`}} href="/">Hanz</a></h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                                </div>
                                <div class="offcanvas-body">
                                    <div class="navbar-nav ms-auto">
                                        <a class="nav-link me-3 text-dark" aria-current="page" href="#ourService">Our Service</a>
                                        <a class="nav-link me-3 text-dark" href="#whyUs">Why Us</a>
                                        <a class="nav-link me-3 text-dark" href="#Testimonial">Testimonial</a>
                                        <a class="nav-link me-3 text-dark" href="#FAQ">FAQ</a>
                                        <a href="/"><button type="button" class="btn" style={{padding:`8px 12px`, fontSize: `15px`, backgroundColor: `#5cb85f`, color:`white`}}>Registered</button></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </nav>
                </div>
                <div class="container-fluid d-flex mt-3 pt-3 mb-5">
                    <div class="row align-items-center">
                        <div class="offset-1 col-6 content1">
                            <div class="blok1">
                                <h1 class="mb-2 fs-2 textContent">Sewa & Rental Mobil Terbaik Di Kawasan Bekasi</h1>
                                <p class="mb-2 textContent">Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.</p>
                                <a class="nav-link me-1" href="cars.html"><button type="button" class="btn btnContent" style={{padding:`8px 12px`, fontSize: `15px`, backgroundColor: `#5cb85f`, color:`white`}}>Mulai Sewa Mobil</button></a>
                            </div>
                        </div>
                        <div class="col pe-0 content1">
                            <div class="blok1">
                                <img class="iCar float-end align-items-end" src="assets/images/img_car.png" alt="" />
                            </div>
                        </div>
                    </div>
                </div>
            </header>
        </div>
    );
}

export default About;
