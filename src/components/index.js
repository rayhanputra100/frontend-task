import LandingPage from "./LandingPage";
import Login from "./Login";
import Protected from "./Protected";
import RecipeReviewCard from "./Card";
import ListCars from "./ListCars";
import About from "./About";

export {LandingPage, Login, Protected, RecipeReviewCard, ListCars, About}