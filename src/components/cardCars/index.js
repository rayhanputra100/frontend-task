import React from 'react';

function Cars() {
        return (
            <div>
                <div className="container d-flex justify-content-center pt-5">
                    <div className="row">
                        
                        <div className="sewaMobil justify-content-center">
                            <div className="card shadow p-2 mb-5 bg-body rounded" style={{ width:`95%` }}>
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col-md">
                                            <label className="form-label" htmlFor="typeDriver">Tipe Driver</label>
                                            <div className="input-group mb-3">
                                                <select id="supir" className="form-select">
                                                    <option disabled>Pilih Tipe Driver</option>
                                                    <option value="true">Dengan Sopir</option>
                                                    <option value="false">Tanpa Sopir (Lepas Tangan)</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div className="col-md">
                                            <label className="form-label" htmlFor="tipeDriver">Tanggal</label>
                                            <input type="date" name="date" id="date" className="form-control" placeholder="Pilih Tanggal" />
                                        </div>
                                        <div className="col-md">
                                            <label className="form-label" htmlFor="tipeDriver">Waktu Jemput/Ambil</label>
                                            <input type="time" name="name" id="time" className="form-control" placeholder="Pilih Waktu" />
                                        </div>
                                        <div className="col-md">
                                            <label className="form-label" htmlFor="tipeDriver">Jumlah Penumpang (optional)</label>
                                            <input type="number" name="penumpang" id="penumpang" className="form-control" placeholder="Jumlah Penumpang" />
                                        </div>
                                        <div className="col-md-1">
                                            <button id="load-btn" type="button" className="btn mb-3 btn-outline-primary" style={{padding:`6px 18px`, fontSize: `18px`}}>Load</button>
                                            <button id="clear-btn" type="button" className="btn btn-outline-primary" style={{padding:`6px 18px`, fontSize: `18px`}}>Clear</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
    
                        
                        <div className="container">
                            <div className="container">
                                <div className="row" id="cars-container"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
}

export default Cars;
