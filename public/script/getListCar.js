class Car {
    static list = [];

    static init(cars) {
        this.list = cars.map((i) => new this(i));
    }

    constructor({ id, plate, manufacture, model, image, rentPerDay, capacity, description, transmission, available, type, year, options, specs, availableAt }) {
        this.id = id;
        this.plate = plate;
        this.manufacture = manufacture;
        this.model = model;
        this.image = image;
        this.rentPerDay = rentPerDay;
        this.capacity = capacity;
        this.description = description;
        this.transmission = transmission;
        this.available = available;
        this.type = type;
        this.year = year;
        this.options = options;
        this.specs = specs;
        this.availableAt = availableAt;
    }

    render() {
        return `
        <div class="card mb-3">
          <img src="${this.image}" class="card-img-top" height="350" alt="${this.manufacture}">
          <div class="card-body">
            <p class="card-text">${this.model}</p>
            <h5 class="card-title">Rp.${this.rentPerDay} / Hari</h5>
            <p class="card-text">${this.description}</p>
            <p class="card-text"><img src="assets/images/fi_users.png" class="me-2" alt="Users">${this.capacity}</p>
            <p class="card-text"><img src="assets/images/fi_settings.png" class="me-2" alt="Transmission">${this.transmission}</p>
            <p class="card-text"><img src="assets/images/fi_calendar.png" class="me-2" alt="Calender">${this.year}</p>
            <a href="#" class="btn btn-primary d-flex justify-content-center">Pilih Mobil</a>
          </div>
        </div>
`;
    }
}